package swen90006.machine;

import java.util.List;
import java.util.ArrayList;
import java.nio.charset.Charset;
import java.nio.file.Path;
import java.nio.file.Files;
import java.nio.file.FileSystems;

import org.junit.*;
import static org.junit.Assert.*;

public class BoundaryTests
{
  //Any method annotated with "@Before" will be executed before each test,
  //allowing the tester to set up some shared resources.
  @Before public void setUp()
  {
  }

  //Any method annotated with "@After" will be executed after each test,
  //allowing the tester to release any shared resources used in the setup.
  @After public void tearDown()
  {
  }

  // lines = 0
  @Test(expected = swen90006.machine.NoReturnValueException.class)
  public void zeroLines()
  {
    final List<String> lines = readInstructions("examples/empty.s");
    Machine m = new Machine();
    m.execute(lines);
  }

  // lines = 1
  @Test public void oneLine()
  {
    final List<String> lines = readInstructions("examples/oneline.s");
    Machine m = new Machine();
    int result = m.execute(lines);
    Assert.assertEquals(result, 0);
  }

  // command not a subset of ["ADD" .. "JZ]
  @Test(expected = swen90006.machine.InvalidInstructionException.class)
  public void invalidCommand()
  {
    final List<String> lines = readInstructions("examples/invalidcommand.s");
    Machine m = new Machine();
    m.execute(lines);
  }

  // ADD words = 3, Ra in R, program contains RET
  @Test public void add()
  {
    final List<String> lines = readInstructions("examples/add.s");
    Machine m = new Machine();
    int result = m.execute(lines);
    Assert.assertEquals(result, 7);
  }

  // ADD words < 3
  @Test(expected = swen90006.machine.InvalidInstructionException.class)
  public void addShort()
  {
    final List<String> lines = readInstructions("examples/addshort.s");
    Machine m = new Machine();
    m.execute(lines);
  }

  // ADD words > 3
  @Test(expected = swen90006.machine.InvalidInstructionException.class)
  public void addLong()
  {
    final List<String> lines = readInstructions("examples/addlong.s");
    Machine m = new Machine();
    m.execute(lines);
  }

  // Program does not contain RET
  @Test(expected = swen90006.machine.NoReturnValueException.class)
  public void noret()
  {
    final List<String> lines = readInstructions("examples/noret.s");
    Machine m = new Machine();
    m.execute(lines);
  }

  // SUB words = 3
  @Test public void sub()
  {
    final List<String> lines = readInstructions("examples/sub.s");
    Machine m = new Machine();
    int result = m.execute(lines);
    Assert.assertEquals(result, -3);
  }

  // SUB words < 3
  @Test(expected = swen90006.machine.InvalidInstructionException.class)
  public void subShort()
  {
    final List<String> lines = readInstructions("examples/subshort.s");
    Machine m = new Machine();
    m.execute(lines);
  }

  // SUB words > 3
  @Test(expected = swen90006.machine.InvalidInstructionException.class)
  public void subLong()
  {
    final List<String> lines = readInstructions("examples/sublong.s");
    Machine m = new Machine();
    m.execute(lines);
  }

  // MUL words = 3
  @Test public void mul()
  {
    final List<String> lines = readInstructions("examples/mul.s");
    Machine m = new Machine();
    int result = m.execute(lines);
    Assert.assertEquals(result, 12);
  }

  // MUL words < 3
  @Test(expected = swen90006.machine.InvalidInstructionException.class)
  public void mulShort()
  {
    final List<String> lines = readInstructions("examples/mulshort.s");
    Machine m = new Machine();
    m.execute(lines);
  }

  // MUL words > 3
  @Test(expected = swen90006.machine.InvalidInstructionException.class)
  public void mulLong()
  {
    final List<String> lines = readInstructions("examples/mullong.s");
    Machine m = new Machine();
    m.execute(lines);
  }

  // DIV words = 3, Rc != 0
  @Test public void div()
  {
    final List<String> lines = readInstructions("examples/div.s");
    Machine m = new Machine();
    int result = m.execute(lines);
    Assert.assertEquals(result, 2);
  }

  // DIV words < 3
  @Test(expected = swen90006.machine.InvalidInstructionException.class)
  public void divShort()
  {
    final List<String> lines = readInstructions("examples/divshort.s");
    Machine m = new Machine();
    m.execute(lines);
  }

  // DIV words > 3
  @Test(expected = swen90006.machine.InvalidInstructionException.class)
  public void divLong()
  {
    final List<String> lines = readInstructions("examples/divlong.s");
    Machine m = new Machine();
    m.execute(lines);
  }

  // Rc = 0 (divide by zero)
  @Test public void divzero()
  {
    final List<String> lines = readInstructions("examples/divzero.s");
    Machine m = new Machine();
    int result = m.execute(lines);
    Assert.assertEquals(result, 17);
  }

  // MOV words = 2
  @Test public void mov()
  {
    final List<String> lines = readInstructions("examples/mov.s");
    Machine m = new Machine();
    int result = m.execute(lines);
    Assert.assertEquals(result, 5);
  }

  // MOV words < 2
  @Test(expected = swen90006.machine.InvalidInstructionException.class)
  public void movShort()
  {
    final List<String> lines = readInstructions("examples/movshort.s");
    Machine m = new Machine();
    m.execute(lines);
  }

  // MOV words > 2
  @Test(expected = swen90006.machine.InvalidInstructionException.class)
  public void movLong()
  {
    final List<String> lines = readInstructions("examples/movlong.s");
    Machine m = new Machine();
    m.execute(lines);
  }

  // register > 31
  @Test(expected = swen90006.machine.InvalidInstructionException.class)
  public void invalidRegister()
  {
    final List<String> lines = readInstructions("examples/invalidregister.s");
    Machine m = new Machine();
    m.execute(lines);
  }

  // register = 31
  @Test public void register30()
  {
    final List<String> lines = readInstructions("examples/register30.s");
    Machine m = new Machine();
    int result = m.execute(lines);
    Assert.assertEquals(result, 7);
  }

  // val < INT_MIN
  @Test(expected = swen90006.machine.InvalidInstructionException.class)
  public void intSmall()
  {
    final List<String> lines = readInstructions("examples/intsmall.s");
    Machine m = new Machine();
    m.execute(lines);
  }

  // val > INT_MAX
  @Test(expected = swen90006.machine.InvalidInstructionException.class)
  public void intLarge()
  {
    final List<String> lines = readInstructions("examples/intlarge.s");
    Machine m = new Machine();
    m.execute(lines);
  }

  // val = INT_MIN
  @Test public void intMin()
  {
    final List<String> lines = readInstructions("examples/intmin.s");
    Machine m = new Machine();
    int result = m.execute(lines);
    Assert.assertEquals(result, -65535);
  }

  // val = INT_MAX
  @Test public void intMax()
  {
    final List<String> lines = readInstructions("examples/intmax.s");
    Machine m = new Machine();
    int result = m.execute(lines);
    Assert.assertEquals(result, 65535);
  }

  // LDR words = 3, STR words = 3
  @Test public void ldrStr()
  {
    final List<String> lines = readInstructions("examples/ldrstr.s");
    Machine m = new Machine();
    int result = m.execute(lines);
    Assert.assertEquals(result, 13);
  }

  // LDR words < 3
  @Test(expected = swen90006.machine.InvalidInstructionException.class)
  public void ldrShort()
  {
    final List<String> lines = readInstructions("examples/ldrshort.s");
    Machine m = new Machine();
    m.execute(lines);
  }

  // LDR words > 3
  @Test(expected = swen90006.machine.InvalidInstructionException.class)
  public void ldrLong()
  {
    final List<String> lines = readInstructions("examples/ldrlong.s");
    Machine m = new Machine();
    m.execute(lines);
  }

  // LDR b + v > MAX_ADDR
  @Test(expected = swen90006.machine.InvalidInstructionException.class)
  public void ldrBig()
  {
    final List<String> lines = readInstructions("examples/ldrstrbig.s");
    Machine m = new Machine();
    m.execute(lines);
  }

  // LDR b + v < -MAX_ADDR
  @Test(expected = swen90006.machine.InvalidInstructionException.class)
  public void ldrSmall()
  {
    final List<String> lines = readInstructions("examples/ldrstrsmall.s");
    Machine m = new Machine();
    m.execute(lines);
  }

  // LDR b + v = MAX_ADDR
  @Test public void ldrMax()
  {
    final List<String> lines = readInstructions("examples/ldrstrmax.s");
    Machine m = new Machine();
    int result = m.execute(lines);
    Assert.assertEquals(result, 0);
  }

  @Test public void ldrMin()
  {
    final List<String> lines = readInstructions("examples/ldrstrmin.s");
    Machine m = new Machine();
    int result = m.execute(lines);
    Assert.assertEquals(result, 0);
  }

  // STR words < 3
  @Test(expected = swen90006.machine.InvalidInstructionException.class)
  public void strShort()
  {
    final List<String> lines = readInstructions("examples/strshort.s");
    Machine m = new Machine();
    m.execute(lines);
  }

  // STR words > 3
  @Test(expected = swen90006.machine.InvalidInstructionException.class)
  public void strLong()
  {
    final List<String> lines = readInstructions("examples/strlong.s");
    Machine m = new Machine();
    m.execute(lines);
  }

  // JMP words = 1, 0 < pc + val < program length
  @Test public void jmp()
  {
    final List<String> lines = readInstructions("examples/jmp.s");
    Machine m = new Machine();
    int result = m.execute(lines);
    Assert.assertEquals(result, 5);
  }

  // JMP words < 1
  @Test(expected = swen90006.machine.InvalidInstructionException.class)
  public void jmpShort()
  {
    final List<String> lines = readInstructions("examples/jmpshort.s");
    Machine m = new Machine();
    m.execute(lines);
  }

  // JMP words > 1
  @Test(expected = swen90006.machine.InvalidInstructionException.class)
  public void jmpLong()
  {
    final List<String> lines = readInstructions("examples/jmplong.s");
    Machine m = new Machine();
    m.execute(lines);
  }

  // JMP pc + val > program length
  @Test(expected = swen90006.machine.NoReturnValueException.class)
  public void jmpFwdJump()
  {
    final List<String> lines = readInstructions("examples/jmpfwdjump.s");
    Machine m = new Machine();
    m.execute(lines);
  }

  // JMP pc + val < 0
  @Test(expected = swen90006.machine.NoReturnValueException.class)
  public void jmpBackJump()
  {
    final List<String> lines = readInstructions("examples/jmpbackjump.s");
    Machine m = new Machine();
    m.execute(lines);
  }

  // JZ words = 2, Ra == 0, pc + val = program length - 1
  @Test public void jz()
  {
    final List<String> lines = readInstructions("examples/jz.s");
    Machine m = new Machine();
    int result = m.execute(lines);
    Assert.assertEquals(result, 0);
  }

  // JZ words = 2, Ra != 0
  @Test public void jznotzero()
  {
    final List<String> lines = readInstructions("examples/jznotzero.s");
    Machine m = new Machine();
    int result = m.execute(lines);
    Assert.assertEquals(result, 5);
  }

  // jZ words < 2
  @Test(expected = swen90006.machine.InvalidInstructionException.class)
  public void jzShort()
  {
    final List<String> lines = readInstructions("examples/jzshort.s");
    Machine m = new Machine();
    m.execute(lines);
  }

  // JZ words > 2
  @Test(expected = swen90006.machine.InvalidInstructionException.class)
  public void jzLong()
  {
    final List<String> lines = readInstructions("examples/jzlong.s");
    Machine m = new Machine();
    m.execute(lines);
  }

  // JZ Ra==0, pc + val = program length
  @Test(expected = swen90006.machine.NoReturnValueException.class)
  public void jzFwd()
  {
    final List<String> lines = readInstructions("examples/jzfwd.s");
    Machine m = new Machine();
    m.execute(lines);
  }

  // jZ Ra==0, pc + val = -1
  @Test(expected = swen90006.machine.NoReturnValueException.class)
  public void jzBack()
  {
    final List<String> lines = readInstructions("examples/jzback.s");
    Machine m = new Machine();
    m.execute(lines);
  }

  // JZ Ra==0, pc + val = 0
  public void jzZero()
  {
    final List<String> lines = readInstructions("examples/jzzero.s");
    Machine m = new Machine();
    int result = m.execute(lines);
    Assert.assertEquals(result, 15);
  }

  // Check that blank lines are ignored
  @Test public void blankLine()
  {
    final List<String> lines = readInstructions("examples/blankline.s");
    Machine m = new Machine();
    int result = m.execute(lines);
    Assert.assertEquals(result, 7);
  }

  //Read in a file containing a program and convert into a list of
  //string instructions
  private List<String> readInstructions(String file)
  {
    Charset charset = Charset.forName("UTF-8");
    List<String> lines = null;
    try {
      lines = Files.readAllLines(FileSystems.getDefault().getPath(file), charset);
    }
    catch (Exception e){
      System.err.println("Invalid input file! (stacktrace follows)");
      e.printStackTrace(System.err);
      System.exit(1);
    }
    return lines;
  }
}
